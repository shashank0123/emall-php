<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;


class AccountController extends Controller
{
    

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function forgotPassword(Request $request)
    {
        if ($request->email)
            return response()->json(['success' => true, 'data' => 'email sent succefully'], 200);
        else
            return response()->json(['success' => false, 'data' => 'please enter correct email id'], 200);
    }
}
