<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\User;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function index()
    {
        $data = Address::paginate(20);
        return view('admin.Address.listAddress', compact('data'));
    }

    public function add()
    {
        
$users = User::all();

        return view('admin.Address.addAddress', compact('users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['address'] = $data['address'];
$saveData['address2'] = $data['address2'];
$saveData['city'] = $data['city'];
$saveData['state'] = $data['state'];
$saveData['pincode'] = $data['pincode'];

 if (isset($data['is_default'])) {
               $saveData['is_default'] = $data['is_default'];
           }
           else
                $saveData['is_default'] = 0;


            
$saveData['address_type'] = $data['address_type'];


        $Address = Address::create($saveData);

        // return response()->json(['success' => true, 'data' => $Address], 200);
        return redirect('/admin/address')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Address::where('id', $id)->first();$users = User::all();
return view('admin.Address.addAddress', compact('row', 'users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['address'] = $data['address'];
$saveData['address2'] = $data['address2'];
$saveData['city'] = $data['city'];
$saveData['state'] = $data['state'];
$saveData['pincode'] = $data['pincode'];

 if (isset($data['is_default'])) {
               $saveData['is_default'] = $data['is_default'];
           }$saveData['address_type'] = $data['address_type'];

        $row = Address::where('id', $id)->first();
        if ($row){
            $Address = Address::where('id', $id)->update($saveData);
        }
        return redirect('/admin/address')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Address::where('id', $request->id)->delete();
        return redirect('/admin/address');

    }


    public function getData(){
        $data = Address::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
