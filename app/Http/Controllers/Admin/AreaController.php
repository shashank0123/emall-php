<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
    public function index()
    {
        $data = Area::paginate(20);
        return view('admin.Area.listArea', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Area.addArea');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['area_name'] = $data['area_name'];
$saveData['status'] = $data['status'];
$saveData['user_id'] = $data['user_id'];


        $Area = Area::create($saveData);

        // return response()->json(['success' => true, 'data' => $Area], 200);
        return redirect('/admin/area')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Area::where('id', $id)->first();return view('admin.Area.addArea', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['area_name'] = $data['area_name'];
$saveData['status'] = $data['status'];
$saveData['user_id'] = $data['user_id'];

        $row = Area::where('id', $id)->first();
        if ($row){
            $Area = Area::where('id', $id)->update($saveData);
        }
        return redirect('/admin/area')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Area::where('id', $request->id)->delete();
        return redirect('/admin/area');

    }


    public function getData(){
        $data = Area::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
