<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    public function index()
    {
        $data = Brand::paginate(20);
        return view('admin.Brand.listBrand', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Brand.addBrand');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['brand_name'] = $data['brand_name'];

 if (request()->hasFile('brand_image')) {
               $path = request()->file('brand_image')->store(
                   'file', 'public'
               );

               $saveData['brand_image'] = \Storage::disk('public')->url($path);

            }
$saveData['status'] = $data['status'];


        $Brand = Brand::create($saveData);

        // return response()->json(['success' => true, 'data' => $Brand], 200);
        return redirect('/admin/brand')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Brand::where('id', $id)->first();return view('admin.Brand.addBrand', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['brand_name'] = $data['brand_name'];

 if (request()->hasFile('brand_image')) {
               $path = request()->file('brand_image')->store(
                   'file', 'public'
               );

               $saveData['brand_image'] = \Storage::disk('public')->url($path);

            }$saveData['status'] = $data['status'];

        $row = Brand::where('id', $id)->first();
        if ($row){
            $Brand = Brand::where('id', $id)->update($saveData);
        }
        return redirect('/admin/brand')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Brand::where('id', $request->id)->delete();
        return redirect('/admin/brand');

    }


    public function getData(){
        $data = Brand::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
