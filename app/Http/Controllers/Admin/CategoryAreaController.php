<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\CategoryArea;
use App\Models\Category;
use App\Models\Area;
use App\Http\Controllers\Controller;

class CategoryAreaController extends Controller
{
    public function index()
    {
        $data = CategoryArea::paginate(20);
        return view('admin.CategoryArea.listCategoryArea', compact('data'));
    }

    public function add()
    {
        
$categorys = Category::all();
$areas = Area::all();

        return view('admin.CategoryArea.addCategoryArea', compact('categorys','areas'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['category_id'] = $data['category_id'];
$saveData['area_id'] = $data['area_id'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
           else
                $saveData['status'] = 0;


            


        $CategoryArea = CategoryArea::create($saveData);

        // return response()->json(['success' => true, 'data' => $CategoryArea], 200);
        return redirect('/admin/category_area')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = CategoryArea::where('id', $id)->first();$categorys = Category::all();
$areas = Area::all();
return view('admin.CategoryArea.addCategoryArea', compact('row', 'categorys','areas'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['category_id'] = $data['category_id'];
$saveData['area_id'] = $data['area_id'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
        $row = CategoryArea::where('id', $id)->first();
        if ($row){
            $CategoryArea = CategoryArea::where('id', $id)->update($saveData);
        }
        return redirect('/admin/category_area')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = CategoryArea::where('id', $request->id)->delete();
        return redirect('/admin/category_area');

    }


    public function getData(){
        $data = CategoryArea::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
