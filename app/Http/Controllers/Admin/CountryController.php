<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function index()
    {
        $data = Country::paginate(20);
        return view('admin.Country.listCountry', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Country.addCountry');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['country_name'] = $data['country_name'];
$saveData['code'] = $data['code'];
$saveData['status'] = $data['status'];


        $Country = Country::create($saveData);

        // return response()->json(['success' => true, 'data' => $Country], 200);
        return redirect('/admin/country')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Country::where('id', $id)->first();return view('admin.Country.addCountry', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['country_name'] = $data['country_name'];
$saveData['code'] = $data['code'];
$saveData['status'] = $data['status'];

        $row = Country::where('id', $id)->first();
        if ($row){
            $Country = Country::where('id', $id)->update($saveData);
        }
        return redirect('/admin/country')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Country::where('id', $request->id)->delete();
        return redirect('/admin/country');

    }


    public function getData(){
        $data = Country::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
