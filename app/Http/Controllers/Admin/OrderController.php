<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $data = Order::paginate(20);
        return view('admin.Order.listOrder', compact('data'));
    }

    public function add()
    {
        
$users = User::all();

        return view('admin.Order.addOrder', compact('users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['address'] = $data['address'];
$saveData['payment_status'] = $data['payment_status'];
$saveData['payment_type'] = $data['payment_type'];
$saveData['order_status'] = $data['order_status'];
$saveData['amount'] = $data['amount'];
$saveData['discount_coupon'] = $data['discount_coupon'];
$saveData['discount'] = $data['discount'];
$saveData['total'] = $data['total'];


        $Order = Order::create($saveData);

        // return response()->json(['success' => true, 'data' => $Order], 200);
        return redirect('/admin/order')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Order::where('id', $id)->first();$users = User::all();
return view('admin.Order.addOrder', compact('row', 'users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['address'] = $data['address'];
$saveData['payment_status'] = $data['payment_status'];
$saveData['payment_type'] = $data['payment_type'];
$saveData['order_status'] = $data['order_status'];
$saveData['amount'] = $data['amount'];
$saveData['discount_coupon'] = $data['discount_coupon'];
$saveData['discount'] = $data['discount'];
$saveData['total'] = $data['total'];

        $row = Order::where('id', $id)->first();
        if ($row){
            $Order = Order::where('id', $id)->update($saveData);
        }
        return redirect('/admin/order')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Order::where('id', $request->id)->delete();
        return redirect('/admin/order');

    }


    public function getData(){
        $data = Order::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
