<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Product;
use App\Http\Controllers\Controller;

class OrderItemController extends Controller
{
    public function index()
    {
        $data = OrderItem::paginate(20);
        return view('admin.OrderItem.listOrderItem', compact('data'));
    }

    public function add()
    {
        
$orders = Order::all();
$products = Product::all();

        return view('admin.OrderItem.addOrderItem', compact('orders','products'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['order_id'] = $data['order_id'];
$saveData['product_id'] = $data['product_id'];
$saveData['amount'] = $data['amount'];
$saveData['gst'] = $data['gst'];
$saveData['total'] = $data['total'];
$saveData['status'] = $data['status'];


        $OrderItem = OrderItem::create($saveData);

        // return response()->json(['success' => true, 'data' => $OrderItem], 200);
        return redirect('/admin/order_item')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = OrderItem::where('id', $id)->first();$orders = Order::all();
$products = Product::all();
return view('admin.OrderItem.addOrderItem', compact('row', 'orders','products'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['order_id'] = $data['order_id'];
$saveData['product_id'] = $data['product_id'];
$saveData['amount'] = $data['amount'];
$saveData['gst'] = $data['gst'];
$saveData['total'] = $data['total'];
$saveData['status'] = $data['status'];

        $row = OrderItem::where('id', $id)->first();
        if ($row){
            $OrderItem = OrderItem::where('id', $id)->update($saveData);
        }
        return redirect('/admin/order_item')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = OrderItem::where('id', $request->id)->delete();
        return redirect('/admin/order_item');

    }


    public function getData(){
        $data = OrderItem::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
