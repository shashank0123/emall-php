<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProductArea;
use App\Models\Product;
use App\Models\Area;
use App\Http\Controllers\Controller;

class ProductAreaController extends Controller
{
    public function index()
    {
        $data = ProductArea::paginate(20);
        return view('admin.ProductArea.listProductArea', compact('data'));
    }

    public function add()
    {
        
$products = Product::all();
$areas = Area::all();

        return view('admin.ProductArea.addProductArea', compact('products','areas'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['product_id'] = $data['product_id'];
$saveData['area_id'] = $data['area_id'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
           else
                $saveData['status'] = 0;


            


        $ProductArea = ProductArea::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProductArea], 200);
        return redirect('/admin/product_area')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProductArea::where('id', $id)->first();$products = Product::all();
$areas = Area::all();
return view('admin.ProductArea.addProductArea', compact('row', 'products','areas'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['product_id'] = $data['product_id'];
$saveData['area_id'] = $data['area_id'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
        $row = ProductArea::where('id', $id)->first();
        if ($row){
            $ProductArea = ProductArea::where('id', $id)->update($saveData);
        }
        return redirect('/admin/product_area')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProductArea::where('id', $request->id)->delete();
        return redirect('/admin/product_area');

    }


    public function getData(){
        $data = ProductArea::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
