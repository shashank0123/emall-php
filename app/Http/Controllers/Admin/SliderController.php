<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index()
    {
        $data = Slider::paginate(20);
        return view('admin.Slider.listSlider', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Slider.addSlider');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }
$saveData['slider_text'] = $data['slider_text'];
$saveData['start_date'] = $data['start_date'];
$saveData['end_date'] = $data['end_date'];
$saveData['status'] = $data['status'];


        $Slider = Slider::create($saveData);

        // return response()->json(['success' => true, 'data' => $Slider], 200);
        return redirect('/admin/slider')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Slider::where('id', $id)->first();return view('admin.Slider.addSlider', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $saveData['image'] = \Storage::disk('public')->url($path);

            }$saveData['slider_text'] = $data['slider_text'];
$saveData['start_date'] = $data['start_date'];
$saveData['end_date'] = $data['end_date'];
$saveData['status'] = $data['status'];

        $row = Slider::where('id', $id)->first();
        if ($row){
            $Slider = Slider::where('id', $id)->update($saveData);
        }
        return redirect('/admin/slider')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Slider::where('id', $request->id)->delete();
        return redirect('/admin/slider');

    }


    public function getData(){
        $data = Slider::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
