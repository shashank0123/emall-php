<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SocialAccount;
use App\Http\Controllers\Controller;

class SocialAccountController extends Controller
{
    public function index()
    {
        $data = SocialAccount::paginate(20);
        return view('admin.SocialAccount.listSocialAccount', compact('data'));
    }

    public function add()
    {
        

        return view('admin.SocialAccount.addSocialAccount');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['account_name'] = $data['account_name'];
$saveData['base_url'] = $data['base_url'];
$saveData['status'] = $data['status'];


        $SocialAccount = SocialAccount::create($saveData);

        // return response()->json(['success' => true, 'data' => $SocialAccount], 200);
        return redirect('/admin/social_account')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = SocialAccount::where('id', $id)->first();return view('admin.SocialAccount.addSocialAccount', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['account_name'] = $data['account_name'];
$saveData['base_url'] = $data['base_url'];
$saveData['status'] = $data['status'];

        $row = SocialAccount::where('id', $id)->first();
        if ($row){
            $SocialAccount = SocialAccount::where('id', $id)->update($saveData);
        }
        return redirect('/admin/social_account')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = SocialAccount::where('id', $request->id)->delete();
        return redirect('/admin/social_account');

    }


    public function getData(){
        $data = SocialAccount::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
