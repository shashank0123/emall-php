<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\State;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
    public function index()
    {
        $data = State::paginate(20);
        return view('admin.State.listState', compact('data'));
    }

    public function add()
    {
        

        return view('admin.State.addState');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['country_id'] = $data['country_id'];
$saveData['state_name'] = $data['state_name'];
$saveData['state_code'] = $data['state_code'];
$saveData['status'] = $data['status'];


        $State = State::create($saveData);

        // return response()->json(['success' => true, 'data' => $State], 200);
        return redirect('/admin/state')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = State::where('id', $id)->first();return view('admin.State.addState', compact('row' ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['country_id'] = $data['country_id'];
$saveData['state_name'] = $data['state_name'];
$saveData['state_code'] = $data['state_code'];
$saveData['status'] = $data['status'];

        $row = State::where('id', $id)->first();
        if ($row){
            $State = State::where('id', $id)->update($saveData);
        }
        return redirect('/admin/state')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = State::where('id', $request->id)->delete();
        return redirect('/admin/state');

    }


    public function getData(){
        $data = State::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
