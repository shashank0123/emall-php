<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;


class CartController extends Controller
{
    

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function add_to_cart(Request $request)
    {
        $products = Product::all();
        $words = array();
        foreach ($products as $key => $value) {
            array_push($words, $value->product_name);
        }
        return response()->json(['success' => true, 'data' => 'product successfully added'], 200);
    }


    public function show_cart(Request $request)
    {
        $products = Product::all();
        $words = array();
        foreach ($products as $key => $value) {
            array_push($words, $value->product_name);
        }
        return response()->json(['success' => true, 'data' => $products], 200);
    }

    
}
