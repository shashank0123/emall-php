<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;


class CategoryController extends Controller
{
    

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function getCategories(Request $request)
    {
        $categories = Category::all();
        return response()->json(['success' => true, 'data' => $categories], 200);
    }
}
