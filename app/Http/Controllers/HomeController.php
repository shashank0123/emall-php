<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_type = \Auth::user()->user_type;
        // $status = \Auth::user()->status;
        $status = '\Auth::user()->status';

        if($user_type == 'subadmin' && $status=='active'){
            return view('subAdmin/admin_dashboard');
        }

        elseif($user_type == 'admin' && $status=='active'){
            return view('admin/admin_dashboard');
        }
        elseif($user_type == 'user' && $status=='active'){
            return view('Users.admin_dashboard', compact('vehicles', 'drivers','vehicle_drivers'));
        }
        elseif($user_type == 'manager'){
            return view('manager.dashboard');
        }
        else{
            session()->flush();
            return redirect()->route('login')->with('message','Your account has been blocked. Contact To Admin.');
        }
    }

}
