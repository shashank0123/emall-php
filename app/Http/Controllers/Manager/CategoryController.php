<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::paginate(20);
        return view('manager.Category.listCategory', compact('data'));
    }

    public function add()
    {
        
$categorys = Category::all();

        return view('manager.Category.addCategory', compact('categorys'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['category_name'] = $data['category_name'];

 if (request()->hasFile('category_image')) {
               $path = request()->file('category_image')->store(
                   'file', 'public'
               );

               $data['category_image'] = \Storage::disk('public')->url($path);

            }
$saveData['category_desc'] = $data['category_desc'];
$saveData['category_slug'] = $data['category_slug'];
$saveData['parent_category'] = $data['parent_category'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
           else
                $saveData['status'] = 0;


            


        $Category = Category::create($saveData);

        // return response()->json(['success' => true, 'data' => $Category], 200);
        return redirect('/manager/category')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Category::where('id', $id)->first();$categorys = Category::all();
return view('manager.Category.addCategory', compact('row', 'categorys'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['category_name'] = $data['category_name'];

 if (request()->hasFile('category_image')) {
               $path = request()->file('category_image')->store(
                   'file', 'public'
               );

               $saveData['category_image'] = \Storage::disk('public')->url($path);

            }$saveData['category_desc'] = $data['category_desc'];
$saveData['category_slug'] = $data['category_slug'];
$saveData['parent_category'] = $data['parent_category'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
        $row = Category::where('id', $id)->first();
        if ($row){
            $Category = Category::where('id', $id)->update($saveData);
        }
        return redirect('/manager/category')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Category::where('id', $request->id)->delete();
        return redirect('/manager/category');

    }


    public function getData(){
        $data = Category::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
