<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Models\ProductAttribute;
use App\Models\User;
use App\Http\Controllers\Controller;

class ProductAttributeController extends Controller
{
    public function index()
    {
        $data = ProductAttribute::paginate(20);
        return view('manager.ProductAttribute.listProductAttribute', compact('data'));
    }

    public function add()
    {
        
$users = User::all();

        return view('manager.ProductAttribute.addProductAttribute', compact('users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['product_id'] = $data['product_id'];
$saveData['attribute_name'] = $data['attribute_name'];
$saveData['attribute_value'] = $data['attribute_value'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
           else
                $saveData['status'] = 0;


            
$saveData['added_by'] = $data['added_by'];


        $ProductAttribute = ProductAttribute::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProductAttribute], 200);
        return redirect('/manager/product_attribute')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProductAttribute::where('id', $id)->first();$users = User::all();
return view('manager.ProductAttribute.addProductAttribute', compact('row', 'users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['product_id'] = $data['product_id'];
$saveData['attribute_name'] = $data['attribute_name'];
$saveData['attribute_value'] = $data['attribute_value'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }$saveData['added_by'] = $data['added_by'];

        $row = ProductAttribute::where('id', $id)->first();
        if ($row){
            $ProductAttribute = ProductAttribute::where('id', $id)->update($saveData);
        }
        return redirect('/manager/product_attribute')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProductAttribute::where('id', $request->id)->delete();
        return redirect('/manager/product_attribute');

    }


    public function getData(){
        $data = ProductAttribute::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
