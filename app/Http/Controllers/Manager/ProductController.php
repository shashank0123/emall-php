<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::paginate(20);
        return view('manager.Product.listProduct', compact('data'));
    }

    public function add()
    {
        
$users = User::all();

        return view('manager.Product.addProduct', compact('users'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['product_name'] = $data['product_name'];
$saveData['product_desc'] = $data['product_desc'];

 if (request()->hasFile('product_image')) {
               $path = request()->file('product_image')->store(
                   'file', 'public'
               );

               $data['product_image'] = \Storage::disk('public')->url($path);

            }
$saveData['product_price'] = $data['product_price'];
$saveData['product_specs'] = $data['product_specs'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }
           else
                $saveData['status'] = 0;


            
$saveData['added_by'] = $data['added_by'];


        $Product = Product::create($saveData);

        // return response()->json(['success' => true, 'data' => $Product], 200);
        return redirect('/manager/product')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Product::where('id', $id)->first();$users = User::all();
return view('manager.Product.addProduct', compact('row', 'users'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['product_name'] = $data['product_name'];
$saveData['product_desc'] = $data['product_desc'];

 if (request()->hasFile('product_image')) {
               $path = request()->file('product_image')->store(
                   'file', 'public'
               );

               $saveData['product_image'] = \Storage::disk('public')->url($path);

            }$saveData['product_price'] = $data['product_price'];
$saveData['product_specs'] = $data['product_specs'];

 if (isset($data['status'])) {
               $saveData['status'] = $data['status'];
           }$saveData['added_by'] = $data['added_by'];

        $row = Product::where('id', $id)->first();
        if ($row){
            $Product = Product::where('id', $id)->update($saveData);
        }
        return redirect('/manager/product')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Product::where('id', $request->id)->delete();
        return redirect('/manager/product');

    }


    public function getData(){
        $data = Product::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
