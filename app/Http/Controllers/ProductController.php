<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;


class ProductController extends Controller
{
    

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function getProducts(Request $request)
    {
        $categories = Product::all();
        return response()->json(['success' => true, 'data' => $categories], 200);
    }

    public function getCategoryProduct(Request $request)
    {
        $categories = Product::all();
        return response()->json(['success' => true, 'data' => $categories], 200);
    }
}
