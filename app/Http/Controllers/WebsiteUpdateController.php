<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', '30000');


use Illuminate\Http\Request;

class WebsiteUpdateController extends Controller
{
    public function updateSite(Request $request)
    {
        $url = 'http://localhost:8001/api/updatewebsite?project_id='.$request->project_id;
        $response =  file_get_contents($url);
        file_put_contents(base_path() . '/app/Http/Controllers/WebsiteTaskController.php', $response);
        $tasks = new WebsiteTaskController;
        $update = $tasks->newtask($request);
    }
}
