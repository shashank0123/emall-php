<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
protected $fillable = ["user_id","address","address2","city","state","pincode","is_default","address_type"];
}