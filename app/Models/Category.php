<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
protected $fillable = ["category_name","category_image","category_desc","category_slug","parent_category","status"];
}