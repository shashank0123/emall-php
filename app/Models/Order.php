<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
protected $fillable = ["user_id","address","payment_status","payment_type","order_status","amount","discount_coupon","discount","total"];
}