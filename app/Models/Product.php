<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
protected $fillable = ["product_name","product_desc","product_image","product_price","product_specs","status","added_by","brand_id"];
}