<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
protected $fillable = ["site_name","logo","email","phone","google_analytics","facebook_link","instagram_link","twitter_link","footer","about_us"];
}