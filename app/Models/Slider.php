<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
protected $fillable = ["image","slider_text","start_date","end_date","status"];
}