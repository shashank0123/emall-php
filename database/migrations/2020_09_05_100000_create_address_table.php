<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration

{
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('address')->nullable();
$table->string('address2')->nullable();
$table->string('city')->nullable();
$table->string('state')->nullable();
$table->string('pincode')->nullable();
$table->string('is_default')->nullable();
$table->string('address_type')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
