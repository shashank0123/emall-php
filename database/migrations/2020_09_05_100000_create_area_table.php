<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaTable extends Migration

{
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id();
$table->string('area_name')->nullable();
$table->string('status')->nullable();
$table->string('user_id')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
