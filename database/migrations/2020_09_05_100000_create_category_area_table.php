<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryAreaTable extends Migration

{
    public function up()
    {
        Schema::create('category_areas', function (Blueprint $table) {
            $table->id();
$table->string('category_id')->nullable();
$table->string('area_id')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('category_areas');
    }
}
