<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemTable extends Migration

{
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
$table->string('order_id')->nullable();
$table->string('product_id')->nullable();
$table->string('amount')->nullable();
$table->string('gst')->nullable();
$table->string('total')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
