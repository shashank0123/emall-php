<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration

{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
$table->string('user_id')->nullable();
$table->string('address')->nullable();
$table->string('payment_status')->nullable();
$table->string('payment_type')->nullable();
$table->string('order_status')->nullable();
$table->string('amount')->nullable();
$table->string('discount_coupon')->nullable();
$table->string('discount')->nullable();
$table->string('total')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
