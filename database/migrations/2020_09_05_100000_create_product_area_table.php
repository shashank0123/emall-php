<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAreaTable extends Migration

{
    public function up()
    {
        Schema::create('product_areas', function (Blueprint $table) {
            $table->id();
$table->string('product_id')->nullable();
$table->string('area_id')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('product_areas');
    }
}
