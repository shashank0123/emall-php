<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeTable extends Migration

{
    public function up()
    {
        Schema::create('product_attributes', function (Blueprint $table) {
            $table->id();
$table->string('product_id')->nullable();
$table->string('attribute_name')->nullable();
$table->string('attribute_value')->nullable();
$table->string('status')->nullable();
$table->string('added_by')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('product_attributes');
    }
}
