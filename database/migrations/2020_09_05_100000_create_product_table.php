<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration

{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
$table->string('product_name')->nullable();
$table->text('product_desc')->nullable();
$table->string('product_image')->nullable();
$table->string('product_price')->nullable();
$table->text('product_specs')->nullable();
$table->string('status')->nullable();
$table->string('added_by')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
