<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderTable extends Migration

{
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->id();
$table->string('image')->nullable();
$table->string('slider_text')->nullable();
$table->string('start_date')->nullable();
$table->string('end_date')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
