@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf

        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_id' id='user_id'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->user_id) && $row->user_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Address</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->address)){{$row->address}} @endif' name='address' placeholder='Enter Address'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Address2</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->address2)){{$row->address2}} @endif' name='address2' placeholder='Enter Address2'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>City</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->city)){{$row->city}} @endif' name='city' placeholder='Enter City'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>State</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->state)){{$row->state}} @endif' name='state' placeholder='Enter State'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Pincode</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->pincode)){{$row->pincode}} @endif' name='pincode' placeholder='Enter Pincode'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='d-block' for='chk-ani3'>
                          <input class='checkbox_animated' name='is_default' id='chk-ani3' type='checkbox' data-original-title=' title='>                                                is default
                        </label>
                    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Address Type</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->address_type)){{$row->address_type}} @endif' name='address_type' placeholder='Enter Address Type'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/address' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection