@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/address/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Address </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr>
<th>User Id</th>
<th>Address</th>
<th>Address2</th>
<th>City</th>
<th>State</th>
<th>Pincode</th>
<th>Is Default</th>
<th>Address Type</th>
<th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value)
                        <tr>
<td>{{$value->user_id}}</td>
<td>{{$value->address}}</td>
<td>{{$value->address2}}</td>
<td>{{$value->city}}</td>
<td>{{$value->state}}</td>
<td>{{$value->pincode}}</td>
<td>{{$value->is_default}}</td>
<td>{{$value->address_type}}</td>
<td><a href='/admin/address/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/address/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td>
</tr>
@endforeach
</tbody>



                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection