@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Category Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->category_name)){{$row->category_name}} @endif' name='category_name' placeholder='Enter Category Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='category_image'>category image</label>
                    @if (isset($row->category_image) && $row->category_image!='')<img width='200px' height='200px' src='{{$row->category_image}}'> @endif

                    <div class='col-lg-12'>
                        <input id='category_image' name='category_image' class='input-file' type='file'>
                    </div>
                    </div><div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='category_desc'>category desc</label>
                    <div class='col-lg-12'>
                        <textarea name='category_desc' class='tinyMCE'>@if (isset($row->category_desc)){{$row->category_desc}} @endif</textarea>
                    </div>
                    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Category Slug</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->category_slug)){{$row->category_slug}} @endif' name='category_slug' placeholder='Enter Category Slug'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>

        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Category</label>
            <select class='form-control btn-square' name='parent_category' id='parent_category'>
                    <option value=''>Select</option>
                @foreach ($categorys as $item)
                    <option @if (isset($row->parent_category) && $row->parent_category == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->category_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    <div class='form-group row'>
                    <label class='d-block' for='chk-ani3'>
                          <input class='checkbox_animated' name='status' id='chk-ani3' type='checkbox' data-original-title=' title='>                                                status
                        </label>
                    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/category' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection