@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/category/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Category </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr>
<th>Category Name</th>
<th>Category Image</th>
<th>Category Desc</th>
<th>Category Slug</th>
<th>Parent Category</th>
<th>Status</th>
<th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value)
                        <tr>
<td>{{$value->category_name}}</td>
<td>{{$value->category_image}}</td>
<td>{{$value->category_desc}}</td>
<td>{{$value->category_slug}}</td>
<td>{{$value->parent_category}}</td>
<td>{{$value->status}}</td>
<td><a href='/admin/category/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/category/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td>
</tr>
@endforeach
</tbody>



                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection