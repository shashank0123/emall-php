@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf

        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_id' id='user_id'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->user_id) && $row->user_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Address</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->address)){{$row->address}} @endif' name='address' placeholder='Enter Address'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Payment Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->payment_status)){{$row->payment_status}} @endif' name='payment_status' placeholder='Enter Payment Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Payment Type</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->payment_type)){{$row->payment_type}} @endif' name='payment_type' placeholder='Enter Payment Type'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Order Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->order_status)){{$row->order_status}} @endif' name='order_status' placeholder='Enter Order Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Amount</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->amount)){{$row->amount}} @endif' name='amount' placeholder='Enter Amount'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Discount Coupon</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->discount_coupon)){{$row->discount_coupon}} @endif' name='discount_coupon' placeholder='Enter Discount Coupon'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Discount</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->discount)){{$row->discount}} @endif' name='discount' placeholder='Enter Discount'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Total</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->total)){{$row->total}} @endif' name='total' placeholder='Enter Total'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/order' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection