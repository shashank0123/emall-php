@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Product Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->product_name)){{$row->product_name}} @endif' name='product_name' placeholder='Enter Product Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='product_desc'>product desc</label>
                    <div class='col-lg-12'>
                        <textarea name='product_desc' class='tinyMCE'>@if (isset($row->product_desc)){{$row->product_desc}} @endif</textarea>
                    </div>
                    </div><div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='product_image'>product image</label>
                    @if (isset($row->product_image) && $row->product_image!='')<img width='200px' height='200px' src='{{$row->product_image}}'> @endif

                    <div class='col-lg-12'>
                        <input id='product_image' name='product_image' class='input-file' type='file'>
                    </div>
                    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Product Price</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->product_price)){{$row->product_price}} @endif' name='product_price' placeholder='Enter Product Price'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='product_specs'>product specs</label>
                    <div class='col-lg-12'>
                        <textarea name='product_specs' class='tinyMCE'>@if (isset($row->product_specs)){{$row->product_specs}} @endif</textarea>
                    </div>
                    </div><div class='form-group row'>
                    <label class='d-block' for='chk-ani3'>
                          <input class='checkbox_animated' name='status' id='chk-ani3' type='checkbox' data-original-title=' title='>                                                status
                        </label>
                    </div>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='added_by' id='added_by'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->added_by) && $row->added_by == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Brand</label>
            <select class='form-control btn-square' name='brand_id' id='brand_id'>
                    <option value=''>Select</option>
                @foreach ($brands as $item)
                    <option @if (isset($row->brand_id) && $row->brand_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->brand_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/product' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection