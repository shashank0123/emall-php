@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf

        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Product</label>
            <select class='form-control btn-square' name='product_id' id='product_id'>
                    <option value=''>Select</option>
                @foreach ($products as $item)
                    <option @if (isset($row->product_id) && $row->product_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->product_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Area</label>
            <select class='form-control btn-square' name='area_id' id='area_id'>
                    <option value=''>Select</option>
                @foreach ($areas as $item)
                    <option @if (isset($row->area_id) && $row->area_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->area_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    <div class='form-group row'>
                    <label class='d-block' for='chk-ani3'>
                          <input class='checkbox_animated' name='status' id='chk-ani3' type='checkbox' data-original-title=' title='>                                                status
                        </label>
                    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/product_area' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection