<nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title link-nav" href="/admin/dashboard"><i data-feather="anchor"></i><span>Dashboard</span></a></li>

                  
                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Category</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/category/add'>Create Category</a></li>
                      <li><a href='/admin/category'>Category</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Product</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/product/add'>Create Product</a></li>
                      <li><a href='/admin/product'>Product</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Product Attribute</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/product_attribute/add'>Create Product Attribute</a></li>
                      <li><a href='/admin/product_attribute'>Product Attribute</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Area</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/area/add'>Create Area</a></li>
                      <li><a href='/admin/area'>Area</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Category Area</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/category_area/add'>Create Category Area</a></li>
                      <li><a href='/admin/category_area'>Category Area</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Product Area</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/product_area/add'>Create Product Area</a></li>
                      <li><a href='/admin/product_area'>Product Area</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Address</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/address/add'>Create Address</a></li>
                      <li><a href='/admin/address'>Address</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Order</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/order/add'>Create Order</a></li>
                      <li><a href='/admin/order'>Order</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Order Item</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/order_item/add'>Create Order Item</a></li>
                      <li><a href='/admin/order_item'>Order Item</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Slider</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/slider/add'>Create Slider</a></li>
                      <li><a href='/admin/slider'>Slider</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>SiteSetting</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/sitesetting/add'>Create SiteSetting</a></li>
                      <li><a href='/admin/sitesetting'>SiteSetting</a></li>
                    </ul>
                  </li>
                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>SiteMenu</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/sitemenu/add'>Create SiteMenu</a></li>
                      <li><a href='/admin/sitemenu'>SiteMenu</a></li>
                    </ul>
                  </li>
                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>StaticPage</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/staticpage/add'>Create StaticPage</a></li>
                      <li><a href='/admin/staticpage'>StaticPage</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
