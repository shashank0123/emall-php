<header data-height="60" data-sticky-height="55" data-responsive-height="70" data-transparent-skin="light" data-header-style="1" data-sticky-style="fixed" data-sticky-offset="header" id="mk-header-1" class="mk-header header-style-1 header-align-left toolbar-true menu-hover-5 sticky-style-fixed mk-background-stretch full-header" role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
    <div class="mk-header-holder">
        <div class="mk-header-toolbar">
            <div class="mk-header-toolbar-holder">
                <span class="header-toolbar-contact">
                    <svg class="mk-svg-icon" data-name="mk-moon-phone-3" data-cacheid="icon-5f39dd2968a3f" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M457.153 103.648c53.267 30.284 54.847 62.709 54.849 85.349v3.397c0 5.182-4.469 9.418-9.928 9.418h-120.146c-5.459 0-9.928-4.236-9.928-9.418v-11.453c0-28.605-27.355-33.175-42.449-35.605-15.096-2.426-52.617-4.777-73.48-4.777h-.14300000000000002c-20.862 0-58.387 2.35-73.48 4.777-15.093 2.427-42.449 6.998-42.449 35.605v11.453c0 5.182-4.469 9.418-9.926 9.418h-120.146c-5.457 0-9.926-4.236-9.926-9.418v-3.397c0-22.64 1.58-55.065 54.847-85.349 63.35-36.01 153.929-39.648 201.08-39.648l.077.078.066-.078c47.152 0 137.732 3.634 201.082 39.648zm-201.152 88.352c-28.374 0-87.443 2.126-117.456 38.519-30.022 36.383-105.09 217.481-38.147 217.481h311.201c66.945 0-8.125-181.098-38.137-217.481-30.018-36.393-89.1-38.519-117.461-38.519zm-.001 192c-35.346 0-64-28.653-64-64s28.654-64 64-64c35.347 0 64 28.653 64 64s-28.653 64-64 64z"></path>
                    </svg>
                    <a href="tel:+91-8814871652">+91-8814871652</a> </span><span class="header-toolbar-contact">
                    <svg class="mk-svg-icon" data-name="mk-moon-envelop" data-cacheid="icon-5f39dd2968c85" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M480 64h-448c-17.6 0-32 14.4-32 32v320c0 17.6 14.4 32 32 32h448c17.6 0 32-14.4 32-32v-320c0-17.6-14.4-32-32-32zm-32 64v23l-192 113.143-192-113.143v-23h384zm-384 256v-177.286l192 113.143 192-113.143v177.286h-384z"></path>
                    </svg>
                    <a href="mailto:hello@wanderon.in">hello@wanderon.in</a>
                </span>
            </div>
        </div>
        <div class="mk-header-inner add-header-height">
            <div class="mk-header-bg"></div>
            <div class="mk-toolbar-resposnive-icon">
                <svg class="mk-svg-icon" data-name="mk-icon-chevron-down" data-cacheid="icon-5f39dd2968e68" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
                    <path d="M1683 808l-742 741q-19 19-45 19t-45-19l-742-741q-19-19-19-45.5t19-45.5l166-165q19-19 45-19t45 19l531 531 531-531q19-19 45-19t45 19l166 165q19 19 19 45.5t-19 45.5z"></path>
                </svg>
            </div>
            <div class="mk-header-nav-container one-row-style menu-hover-style-5" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                <nav class="mk-main-navigation js-main-nav">
                    <ul id="menu-main-menu" class="main-navigation-ul dropdownJavascript">
                        <li id="menu-item-43100" class="menu-item menu-item-type-custom menu-item-object-custom has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.workcations.in/">WORKCATIONS</a>
                        </li>
                        <li id="menu-item-42974" class="menu-item menu-item-type-post_type menu-item-object-page has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/sanitised-taxi-services/">Book Sanitised Taxi</a>
                        </li>
                        <li id="menu-item-16417" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children no-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/backpacking-trips/" aria-haspopup="true">Backpacking Trips</a>
                            <ul style="" class="sub-menu">
                                <li id="menu-item-38310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children with-menu">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/leh-ladakh-trips/" aria-haspopup="true">Leh Ladakh</a><i class="menu-sub-level-arrow"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5f39dd296bf73" style="height: 16px; width: 5.7142857142857px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792">
                                            <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                        </svg></i>
                                    <ul style="" class="sub-menu">
                                        <li id="menu-item-38311" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/srinagar-leh-srinagar/">Srinagar Leh Srinagar</a>
                                        </li>
                                        <li id="menu-item-38312" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/manali-leh-srinagar/">Manali Leh Srinagar</a>
                                        </li>
                                        <li id="menu-item-38313" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/srinagar-leh-manali/">Srinagar Leh Manali</a>
                                        </li>
                                        <li id="menu-item-38314" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/ladakh/">Manali Leh Manali</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-38315" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children with-menu">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spititrip/" aria-haspopup="true">Spiti Valley</a><i class="menu-sub-level-arrow"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5f39dd296c4ac" style="height: 16px; width: 5.7142857142857px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792">
                                            <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                        </svg></i>
                                    <ul style="" class="sub-menu">
                                        <li id="menu-item-38316" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitiwinter/">Spiti Valley Winter Expedition</a>
                                        </li>
                                        <li id="menu-item-38317" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitiwinterbiking/">Spiti Winter Biking</a>
                                        </li>
                                        <li id="menu-item-38318" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitivalley/">Spiti Valley RoadTrip</a>
                                        </li>
                                        <li id="menu-item-38319" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitibiking/">Spiti Valley Biking Expedition</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-38414" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/bali/">Bali</a>
                                </li>
                                <li id="menu-item-16418" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/bhutan">Bhutan</a>
                                </li>
                                <li id="menu-item-38159" class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/tawangroadtrip/">Tawang Road Trip</a>
                                </li>
                                <li id="menu-item-3676" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/meghalayaroadtrip">Meghalaya</a>
                                </li>
                                <li id="menu-item-32096" class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/sikkim-roadtrip/">Sikkim Road Trip</a>
                                </li>
                            </ul>
                        </li>
                        <li id="menu-item-31718" class="menu-item menu-item-type-custom menu-item-object-custom has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/weekendgetaways">Weekend Delhi</a>
                        </li>
                        <li id="menu-item-31719" class="menu-item menu-item-type-custom menu-item-object-custom has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/weekend-getaways-from-bangalore">Weekend Bangalore</a>
                        </li>
                        <li id="menu-item-5930" class="menu-item menu-item-type-custom menu-item-object-custom no-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/blogs">Blogs</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="mk-nav-responsive-link">
                <div class="mk-css-icon-menu">
                    <div class="mk-css-icon-menu-line-1"></div>
                    <div class="mk-css-icon-menu-line-2"></div>
                    <div class="mk-css-icon-menu-line-3"></div>
                </div>
            </div>
            <div class="header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky">
                <a href="https://www.wanderon.in/" title="WanderOn"><img class="mk-desktop-logo dark-logo" title="Travel Community" alt="Travel Community" src="https://www.wanderon.in/wp-content/uploads/2018/11/black.png" /><img class="mk-desktop-logo light-logo" title="Travel Community" alt="Travel Community" src="https://www.wanderon.in/wp-content/uploads/2018/11/white.png" />
                    <img class="mk-resposnive-logo" title="Travel Community" alt="Travel Community" src="https://www.wanderon.in/wp-content/uploads/2018/11/black.png" />
                    <img class="mk-sticky-logo" title="Travel Community" alt="Travel Community" src="https://www.wanderon.in/wp-content/uploads/2018/11/black.png" />
                </a>
            </div>
            <div class="mk-header-right"></div>
        </div>
        <div class="mk-responsive-wrap">
            <nav class="menu-main-menu-container">
                <ul id="menu-main-menu-1" class="mk-responsive-nav">
                    <li id="responsive-menu-item-43100" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="https://www.workcations.in/">WORKCATIONS</a>
                    </li>
                    <li id="responsive-menu-item-42974" class="menu-item menu-item-type-post_type menu-item-object-page">
                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/sanitised-taxi-services/">Book Sanitised Taxi</a>
                    </li>
                    <li id="responsive-menu-item-16417" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/backpacking-trips/">Backpacking Trips</a><span class="mk-nav-arrow mk-nav-sub-closed"><svg class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5f39dd296f221" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"></path>
                            </svg></span>
                        <ul class="sub-menu">
                            <li id="responsive-menu-item-38310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/leh-ladakh-trips/">Leh Ladakh</a><span class="mk-nav-arrow mk-nav-sub-closed"><svg class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5f39dd296f60c" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"></path>
                                    </svg></span>
                                <ul class="sub-menu">
                                    <li id="responsive-menu-item-38311" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/srinagar-leh-srinagar/">Srinagar Leh Srinagar</a>
                                    </li>
                                    <li id="responsive-menu-item-38312" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/manali-leh-srinagar/">Manali Leh Srinagar</a>
                                    </li>
                                    <li id="responsive-menu-item-38313" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/srinagar-leh-manali/">Srinagar Leh Manali</a>
                                    </li>
                                    <li id="responsive-menu-item-38314" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/ladakh/">Manali Leh Manali</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="responsive-menu-item-38315" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spititrip/">Spiti Valley</a><span class="mk-nav-arrow mk-nav-sub-closed"><svg class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5f39dd296fbeb" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"></path>
                                    </svg></span>
                                <ul class="sub-menu">
                                    <li id="responsive-menu-item-38316" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitiwinter/">Spiti Valley Winter Expedition</a>
                                    </li>
                                    <li id="responsive-menu-item-38317" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitiwinterbiking/">Spiti Winter Biking</a>
                                    </li>
                                    <li id="responsive-menu-item-38318" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitivalley/">Spiti Valley RoadTrip</a>
                                    </li>
                                    <li id="responsive-menu-item-38319" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/spitibiking/">Spiti Valley Biking Expedition</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="responsive-menu-item-38414" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/bali/">Bali</a>
                            </li>
                            <li id="responsive-menu-item-16418" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/bhutan">Bhutan</a>
                            </li>
                            <li id="responsive-menu-item-38159" class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/tawangroadtrip/">Tawang Road Trip</a>
                            </li>
                            <li id="responsive-menu-item-3676" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/meghalayaroadtrip">Meghalaya</a>
                            </li>
                            <li id="responsive-menu-item-32096" class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/sikkim-roadtrip/">Sikkim Road Trip</a>
                            </li>
                        </ul>
                    </li>
                    <li id="responsive-menu-item-31718" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/weekendgetaways">Weekend Delhi</a>
                    </li>
                    <li id="responsive-menu-item-31719" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/weekend-getaways-from-bangalore">Weekend Bangalore</a>
                    </li>
                    <li id="responsive-menu-item-5930" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="https://www.wanderon.in/blogs">Blogs</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="mk-header-padding-wrapper"></div>
</header>