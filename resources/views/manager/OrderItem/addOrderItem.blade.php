@extends('manager.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Order</label>
            <select class='form-control btn-square' name='order_id' id='order_id'>
                    <option value=''>Select</option>
                @foreach ($orders as $item)
                    <option @if (isset($row->order_id) && $row->order_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->user_id}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Product</label>
            <select class='form-control btn-square' name='product_id' id='product_id'>
                    <option value=''>Select</option>
                @foreach ($products as $item)
                    <option @if (isset($row->product_id) && $row->product_id == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->product_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Amount</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->amount)){{$row->amount}} @endif' name='amount' placeholder='Enter Amount'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Gst</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->gst)){{$row->gst}} @endif' name='gst' placeholder='Enter Gst'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Total</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->total)){{$row->total}} @endif' name='total' placeholder='Enter Total'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->status)){{$row->status}} @endif' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/order_item' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection