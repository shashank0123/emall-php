@extends('manager.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Product Id</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->product_id)){{$row->product_id}} @endif' name='product_id' placeholder='Enter Product Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Attribute Name</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->attribute_name)){{$row->attribute_name}} @endif' name='attribute_name' placeholder='Enter Attribute Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Attribute Value</label>
                <input class='form-control btn-square' type='text' value='@if (isset($row->attribute_value)){{$row->attribute_value}} @endif' name='attribute_value' placeholder='Enter Attribute Value'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='d-block' for='chk-ani3'>
                          <input class='checkbox_animated' name='status' id='chk-ani3' type='checkbox' data-original-title=' title='>                                                status
                        </label>
                    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='added_by' id='added_by'>
                    <option value=''>Select</option>
                @foreach ($users as $item)
                    <option @if (isset($row->added_by) && $row->added_by == $item->id){{'selected'}} @endif value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/product_attribute' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection