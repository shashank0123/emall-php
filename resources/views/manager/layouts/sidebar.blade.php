<nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title link-nav" href="/manager/dashboard"><i data-feather="anchor"></i><span>Dashboard</span></a></li>

                  
                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Category</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/manager/category/add'>Create Category</a></li>
                      <li><a href='/manager/category'>Category</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Product</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/manager/product/add'>Create Product</a></li>
                      <li><a href='/manager/product'>Product</a></li>
                    </ul>
                  </li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Product Attribute</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/manager/product_attribute/add'>Create Product Attribute</a></li>
                      <li><a href='/manager/product_attribute'>Product Attribute</a></li>
                    </ul>
                  </li>

                  
                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
