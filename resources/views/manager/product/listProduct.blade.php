@extends('manager.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/product/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Product </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Product Name</th><th>Product Desc</th><th>Product Image</th><th>Product Price</th><th>Product Specs</th><th>Status</th><th>Added By</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) 
                        <tr><td>{{$value->product_name}}</td><td>{{$value->product_desc}}</td><td>{{$value->product_image}}</td><td>{{$value->product_price}}</td><td>{{$value->product_specs}}</td><td>{{$value->status}}</td><td>{{$value->added_by}}</td><td><a href='/admin/product/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/product/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection