<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('updatewebsite', 'WebsiteUpdateController@updateSite');


Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/home', 'Admin\AdminController@index')->name('admin.home');


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/dashboard', 'Admin\DashboardController@getDashboard');
Route::get('/slider','Admin\SliderController@index');
    Route::get('slider/add','Admin\SliderController@add');
    Route::post('slider/add','Admin\SliderController@store');
    Route::get('slider/edit/{id}','Admin\SliderController@edit');
    Route::post('slider/edit/{id}','Admin\SliderController@update');
    Route::get('slider/delete/{id}','Admin\SliderController@delete');  
Route::get('/sitesetting','Admin\SiteSettingController@index');
    Route::get('sitesetting/add','Admin\SiteSettingController@add');
    Route::post('sitesetting/add','Admin\SiteSettingController@store');
    Route::get('sitesetting/edit/{id}','Admin\SiteSettingController@edit');
    Route::post('sitesetting/edit/{id}','Admin\SiteSettingController@update');
    Route::get('sitesetting/delete/{id}','Admin\SiteSettingController@delete');  
Route::get('/sitemenu','Admin\SiteMenuController@index');
    Route::get('sitemenu/add','Admin\SiteMenuController@add');
    Route::post('sitemenu/add','Admin\SiteMenuController@store');
    Route::get('sitemenu/edit/{id}','Admin\SiteMenuController@edit');
    Route::post('sitemenu/edit/{id}','Admin\SiteMenuController@update');
    Route::get('sitemenu/delete/{id}','Admin\SiteMenuController@delete');  
Route::get('/staticpage','Admin\StaticPageController@index');
    Route::get('staticpage/add','Admin\StaticPageController@add');
    Route::post('staticpage/add','Admin\StaticPageController@store');
    Route::get('staticpage/edit/{id}','Admin\StaticPageController@edit');
    Route::post('staticpage/edit/{id}','Admin\StaticPageController@update');
    Route::get('staticpage/delete/{id}','Admin\StaticPageController@delete');  

Route::get('/category','Admin\CategoryController@index');
    Route::get('category/add','Admin\CategoryController@add');
    Route::post('category/add','Admin\CategoryController@store');
    Route::get('category/edit/{id}','Admin\CategoryController@edit');
    Route::post('category/edit/{id}','Admin\CategoryController@update');
    Route::get('category/delete/{id}','Admin\CategoryController@delete');  
Route::get('/product','Admin\ProductController@index');
    Route::get('product/add','Admin\ProductController@add');
    Route::post('product/add','Admin\ProductController@store');
    Route::get('product/edit/{id}','Admin\ProductController@edit');
    Route::post('product/edit/{id}','Admin\ProductController@update');
    Route::get('product/delete/{id}','Admin\ProductController@delete');  
Route::get('/product_attribute','Admin\ProductAttributeController@index');
    Route::get('product_attribute/add','Admin\ProductAttributeController@add');
    Route::post('product_attribute/add','Admin\ProductAttributeController@store');
    Route::get('product_attribute/edit/{id}','Admin\ProductAttributeController@edit');
    Route::post('product_attribute/edit/{id}','Admin\ProductAttributeController@update');
    Route::get('product_attribute/delete/{id}','Admin\ProductAttributeController@delete');  
Route::get('/category_area','Admin\CategoryAreaController@index');
    Route::get('category_area/add','Admin\CategoryAreaController@add');
    Route::post('category_area/add','Admin\CategoryAreaController@store');
    Route::get('category_area/edit/{id}','Admin\CategoryAreaController@edit');
    Route::post('category_area/edit/{id}','Admin\CategoryAreaController@update');
    Route::get('category_area/delete/{id}','Admin\CategoryAreaController@delete');  
Route::get('/product_area','Admin\ProductAreaController@index');
    Route::get('product_area/add','Admin\ProductAreaController@add');
    Route::post('product_area/add','Admin\ProductAreaController@store');
    Route::get('product_area/edit/{id}','Admin\ProductAreaController@edit');
    Route::post('product_area/edit/{id}','Admin\ProductAreaController@update');
    Route::get('product_area/delete/{id}','Admin\ProductAreaController@delete');  
Route::get('/address','Admin\AddressController@index');
    Route::get('address/add','Admin\AddressController@add');
    Route::post('address/add','Admin\AddressController@store');
    Route::get('address/edit/{id}','Admin\AddressController@edit');
    Route::post('address/edit/{id}','Admin\AddressController@update');
    Route::get('address/delete/{id}','Admin\AddressController@delete');  
Route::get('/order','Admin\OrderController@index');
    Route::get('order/add','Admin\OrderController@add');
    Route::post('order/add','Admin\OrderController@store');
    Route::get('order/edit/{id}','Admin\OrderController@edit');
    Route::post('order/edit/{id}','Admin\OrderController@update');
    Route::get('order/delete/{id}','Admin\OrderController@delete');  
Route::get('/order_item','Admin\OrderItemController@index');
    Route::get('order_item/add','Admin\OrderItemController@add');
    Route::post('order_item/add','Admin\OrderItemController@store');
    Route::get('order_item/edit/{id}','Admin\OrderItemController@edit');
    Route::post('order_item/edit/{id}','Admin\OrderItemController@update');
    Route::get('order_item/delete/{id}','Admin\OrderItemController@delete');  
Route::get('/site_setting','Admin\SiteSettingController@index');
    Route::get('site_setting/add','Admin\SiteSettingController@add');
    Route::post('site_setting/add','Admin\SiteSettingController@store');
    Route::get('site_setting/edit/{id}','Admin\SiteSettingController@edit');
    Route::post('site_setting/edit/{id}','Admin\SiteSettingController@update');
    Route::get('site_setting/delete/{id}','Admin\SiteSettingController@delete');  
Route::get('/site_menu','Admin\SiteMenuController@index');
    Route::get('site_menu/add','Admin\SiteMenuController@add');
    Route::post('site_menu/add','Admin\SiteMenuController@store');
    Route::get('site_menu/edit/{id}','Admin\SiteMenuController@edit');
    Route::post('site_menu/edit/{id}','Admin\SiteMenuController@update');
    Route::get('site_menu/delete/{id}','Admin\SiteMenuController@delete');  
Route::get('/static_page','Admin\StaticPageController@index');
    Route::get('static_page/add','Admin\StaticPageController@add');
    Route::post('static_page/add','Admin\StaticPageController@store');
    Route::get('static_page/edit/{id}','Admin\StaticPageController@edit');
    Route::post('static_page/edit/{id}','Admin\StaticPageController@update');
    Route::get('static_page/delete/{id}','Admin\StaticPageController@delete');  
Route::get('/area','Admin\AreaController@index');
    Route::get('area/add','Admin\AreaController@add');
    Route::post('area/add','Admin\AreaController@store');
    Route::get('area/edit/{id}','Admin\AreaController@edit');
    Route::post('area/edit/{id}','Admin\AreaController@update');
    Route::get('area/delete/{id}','Admin\AreaController@delete');  
Route::get('/brand','Admin\BrandController@index');
    Route::get('brand/add','Admin\BrandController@add');
    Route::post('brand/add','Admin\BrandController@store');
    Route::get('brand/edit/{id}','Admin\BrandController@edit');
    Route::post('brand/edit/{id}','Admin\BrandController@update');
    Route::get('brand/delete/{id}','Admin\BrandController@delete');  
//{Route to be added here from automation}
});
