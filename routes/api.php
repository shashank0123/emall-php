<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/auth/register', 'AuthApiController@register');
Route::get('/categories', 'CategoryController@getCategories');
Route::get('/best_selling', 'ProductController@getProducts');
Route::get('/recommended_product', 'ProductController@getProducts');
Route::get('/home_banner', 'SliderController@getSlider');
Route::get('/product_by_category', 'ProductController@getCategoryProduct');
Route::post('/forgot_password', 'AccountController@forgotPassword');
Route::get('/dynamic_search', 'SearchController@dynamic_search');
Route::get('/keyword_search', 'SearchController@keyword_search');
Route::get('/brands', 'BrandController@brands');


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthApiController@login');
    Route::post('logout', 'AuthApiController@logout');
    Route::post('refresh', 'AuthApiController@refresh');
    Route::post('me', 'AuthApiController@me');

    //{Route to be added here from automation}

});


Route::group([
    'middleware' => ['auth:api', 'api']
], function ($router) {

    Route::post('/add_to_cart', 'CartController@add_to_cart');
    Route::get('/show_cart', 'CartController@show_cart');

    //{Route to be added here from automation}

});


