<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('updatewebsite', 'WebsiteUpdateController@updateSite');


Route::get('/login', 'Manager\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Manager\Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/home', 'Manager\AdminController@index')->name('admin.home');


Route::group(['middleware' => ['auth', 'manager']], function () {
    Route::get('/dashboard', 'Manager\DashboardController@getDashboard');
    Route::get('/category','Manager\CategoryController@index');
    Route::get('category/add','Manager\CategoryController@add');
    Route::post('category/add','Manager\CategoryController@store');
    Route::get('category/edit/{id}','Manager\CategoryController@edit');
    Route::post('category/edit/{id}','Manager\CategoryController@update');
    Route::get('category/delete/{id}','Manager\CategoryController@delete');  
    Route::get('/product','Manager\ProductController@index');
    Route::get('product/add','Manager\ProductController@add');
    Route::post('product/add','Manager\ProductController@store');
    Route::get('product/edit/{id}','Manager\ProductController@edit');
    Route::post('product/edit/{id}','Manager\ProductController@update');
    Route::get('product/delete/{id}','Manager\ProductController@delete');  
    Route::get('/product_attribute','Manager\ProductAttributeController@index');
    Route::get('product_attribute/add','Manager\ProductAttributeController@add');
    Route::post('product_attribute/add','Manager\ProductAttributeController@store');
    Route::get('product_attribute/edit/{id}','Manager\ProductAttributeController@edit');
    Route::post('product_attribute/edit/{id}','Manager\ProductAttributeController@update');
    Route::get('product_attribute/delete/{id}','Manager\ProductAttributeController@delete');  
    Route::get('/address','Manager\AddressController@index');
    Route::get('address/add','Manager\AddressController@add');
    Route::post('address/add','Manager\AddressController@store');
    Route::get('address/edit/{id}','Manager\AddressController@edit');
    Route::post('address/edit/{id}','Manager\AddressController@update');
    Route::get('address/delete/{id}','Manager\AddressController@delete');  
    Route::get('/order','Manager\OrderController@index');
    Route::get('order/add','Manager\OrderController@add');
    Route::post('order/add','Manager\OrderController@store');
    Route::get('order/edit/{id}','Manager\OrderController@edit');
    Route::post('order/edit/{id}','Manager\OrderController@update');
    Route::get('order/delete/{id}','Manager\OrderController@delete');  
    Route::get('/order_item','Manager\OrderItemController@index');
    Route::get('order_item/add','Manager\OrderItemController@add');
    Route::post('order_item/add','Manager\OrderItemController@store');
    Route::get('order_item/edit/{id}','Manager\OrderItemController@edit');
    Route::post('order_item/edit/{id}','Manager\OrderItemController@update');
    Route::get('order_item/delete/{id}','Manager\OrderItemController@delete');  
//{Route to be added here from automation}
});
